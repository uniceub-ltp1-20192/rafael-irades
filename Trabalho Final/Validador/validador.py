import re
import os

def cprint(str):
	os.system('cls')
	print(str)

class Validador(object):
	
	@staticmethod
	def validar(expReg, msgErro):
		# Loop eterno até o usuário colocar uma opção válida
		while True:
			op = input("Informe um número: ")
			v = re.match(expReg, op)

			if v != None:
				return op

			print(msgErro.format(expReg))

	@staticmethod
	def validarValorEntrada(valorAtual, msg):
		novoValor = input(msg)

		if novoValor != None and novoValor != "":
			return novoValor

		return valorAtual