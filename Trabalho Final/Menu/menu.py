import os

clear = lambda: os.system('cls')

# Função para limpar a tela antes de dar print
def cprint(str):
	clear()
	print(str)

from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.entidadeFilha import Husky

class Menu:

	@staticmethod
	def menuPrincipal():
		print("""
Escolha uma opção:
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")

		return Validador.validar("[0-4]", "\nOpção do menu deve ser um número entre {}\n")

	@staticmethod
	def menuConsultar():
		print("""
Escolha uma opção:
0 - Voltar
1 - ... por Identificador
2 - ... por Propriedade
""")

		return Validador.validar("[0-2]", "\nOpção do menu deve ser um número entre {}\n")

	@staticmethod
	def iniciarMenu():
		op = ""
		dados = Dados()

		while (op != "0"):
			op = Menu.menuPrincipal()

			# Opção Consultar
			if (op == "1"):
				clear()

				while (op != "0"):
					print("Consultar...")

					op = Menu.menuConsultar()

					if (op == "1"):
						cprint("Consulta por Identificador")

						retorno = Menu.menuBuscarPorIdentificador(dados)

						if retorno != None:
							cprint(retorno)
						else:
							cprint("Não encontrado.\n\n")

					elif (op == "2"):
						cprint("Consulta por Propriedade")
						Menu.menuBuscarPorPropriedade(dados)

					elif (op == "0"):
						cprint("Voltando...")

					else:
						cprint("Opção inválida.")

				op = ""

			# Opção Inserir
			elif (op == "2"):
				cprint("Inserir\n")
				Menu.menuInserir(dados)

			# Opção Alterar
			elif (op == "3"):
				clear()

				while (op != "0"):
					print("Alterar...")

					op = Menu.menuConsultar()

					if (op == "1"):
						cprint("Alterar por Identificador")

						retorno = Menu.menuBuscarPorIdentificador(dados)

						if retorno == None:
							print("Identificador não encontrado.")
						else:
							Menu.menuAlterar(dados, retorno)

					elif (op == "2"):
						cprint("Alterar por Propriedade")
						
						retorno = Menu.menuBuscarPorPropriedade(dados)
						Menu.menuAlterar(dados, retorno)

					elif (op == "0"):
						cprint("Voltando...")

					else:
						cprint("Opção inválida.")

				op = ""

			# Opção Deletar
			elif (op == "4"):
				clear()

				while (op != "0"):
					print("Deletar...")

					op = Menu.menuConsultar()

					if (op == "1"):
						cprint("Deletar por Identificador")

						retorno = Menu.menuBuscarPorIdentificador(dados)
						if retorno != None:
							Menu.menuDeletar(dados, entidade)
						else:
							cprint("Não encontrado.\n\n")

					elif (op == "2"):
						cprint("Deletar por Propriedade")

						retorno = Menu.menuBuscarPorPropriedade(dados)
						if retorno != None:
							Menu.menuDeletar(dados, retorno)
						else:
							cprint("Não encontrado.\n\n")

					elif (op == "0"):
						cprint("Voltando...")

					else:
						cprint("Opção inválida.")

				op = ""

			elif (op == "0"):
				cprint("Saindo...")

			else:
				cprint("Opção inválida.")

	@staticmethod
	def menuBuscarPorIdentificador(dados):
		retorno = dados.buscarPorIdentificador(Validador.validar(r'\d+', ''))

		return retorno

	@staticmethod
	def menuBuscarPorPropriedade(dados):
		retorno = dados.buscarPorPropriedade(input("Informe o peso: "))

		if retorno != None:
			cprint(retorno)
		else:
			cprint("Não encontrado.\n\n")

		return retorno

	@staticmethod
	def menuInserir(dados):
		husky = Husky()
		husky.raca = "Husky"
		husky.tamanho = input("Informe o tamanho: ")
		husky.peso 	  = input("Informe o peso: ")
		husky.idade	  = input("Informe a idade: ")
		husky.cor 	  = input("Informe a cor: ")

		dados.inserirDado(husky)

	@staticmethod
	def menuDeletar(dados, entidade):
		print(entidade)
		print()

		resposta = input("""
Deseja deletar os dados do husky?
S - Sim
N - Não
""")
		if(resposta == "S" or resposta == "s"):
			dados.deletar(entidade)

		print("\n")

	@staticmethod
	def menuAlterar(dados, entidade):
		cprint(entidade)

		entidade.tamanho = Validador.validarValorEntrada(entidade.tamanho, "\nInforme o tamanho: ")
		entidade.peso 	 = Validador.validarValorEntrada(entidade.peso, "Informe um peso: ")
		entidade.idade 	 = Validador.validarValorEntrada(entidade.idade, "Informe a idade: ")
		entidade.cor 	 = Validador.validarValorEntrada(entidade.cor, "Informe uma cor: ")

		print("\n")

		dados.alterarDado(entidade)