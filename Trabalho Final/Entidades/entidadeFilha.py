from Entidades.entidadeMae import Cachorro

class Husky(Cachorro):

	def __init__(self, cor = "Cor não informada"):
		super().__init__()
		self._cor = cor

	@property
	def cor(self):
		return self._cor
	
	@cor.setter
	def cor(self, cor):
		self._cor = cor

	def __str__(self):
		return """Dados do Husky:
Identificador: {}
Tamanho: {}
Peso: {}
Idade: {}
Cor: {}
""".format(self.idt, self.tamanho, self.peso, self.idade, self.cor)