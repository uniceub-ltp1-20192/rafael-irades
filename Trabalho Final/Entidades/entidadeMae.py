class Cachorro:

	def __init__(self, raca = "Husky", tamanho = 0, peso = "", idade = 0, idt = 0):
		self._raca 	  = raca
		self._tamanho = tamanho
		self._peso 	  = peso
		self._idade   = idade
		self._idt     = idt
	
	@property
	def raca(self):
		return self._raca
	
	@raca.setter
	def raca(self, raca):
		self._raca = raca
	
	@property
	def tamanho(self):
		return self._tamanho
	
	@tamanho.setter
	def tamanho(self, tamanho):
		self._tamanho = tamanho

	@property
	def peso(self):
		return self._peso
	
	@peso.setter
	def peso(self, peso):
		self._peso = peso

	@property
	def idade(self):
		return self._idade
	
	@idade.setter
	def idade(self, idade):
		self._idade = idade

	@property
	def idt(self):
		return self._idt

	@idt.setter
	def idt(self, idt):
		self._idt = idt