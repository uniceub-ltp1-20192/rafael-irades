import os

def cprint(str):
	os.system('cls')
	print(str)

from Entidades.entidadeFilha import Husky
from Validador.validador import Validador

class Dados:

	def __init__(self):
		self._dados = dict()
		self._identificador = 0

	def geradorIdentificador(self):
		self._identificador = self._identificador + 1

		return self._identificador

	def buscarPorIdentificador(self, identificador):
		if (len(self._dados) != 0):
			return self._dados.get(int(identificador))

		cprint("Erro: Dicionário vazio.")

	def buscarPorPropriedade(self, param):
		lista = dict()
		ultimoEncontrado = 0

		if (len(self._dados) == 0):
			cprint("Erro: Dicionário vazio.")
		else:
			for x in self._dados.values():
				if x.peso == param:
					lista[x.idt] = x
					ultimoEncontrado = x.idt

			if len(lista) == 1:
				return lista[ultimoEncontrado]
			else:
				if len(lista) == 0:
					cprint("Não encontrado.")
					return None
				return self.buscarPorIdentificadorComLista(lista)

	def inserirDado(self, entidade):
		self._ultimoIdentificador = self.geradorIdentificador()
		entidade.idt = self._ultimoIdentificador

		cprint(entidade)
		
		self._dados[self._ultimoIdentificador] = entidade


	def deletar(self, entidade):
		del self._dados[entidade.idt]

		cprint("Entidade deletada.")

	def alterarDado(self, entidade):
		self._dados[entidade.idt] = entidade

	def buscarPorIdentificadorComLista(self, lista):
		cprint("Existem múltiplos registros com a propriedade informada:\n")

		for x in lista.values():
			print(x)

		print("Informe um identificador: ")

		return lista.get(int(Validador.validar("\d+", "Informe um identificador válido.")))