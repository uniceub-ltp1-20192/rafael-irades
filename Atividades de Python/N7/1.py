def inviteMsg(name):
	msg = "Olá, ", name, "você foi convidado para um jantar."

	return msg

guests = ['Caesar', 'George', 'Erina']

for guest in guests:
	print(inviteMsg(guest))

print("Tem ", len(guests), " convidados no total.")