def inviteMsg(name):
	msg = "Olá, ", name, "você foi convidado para um jantar."

	return msg

guests = ['George', 'Erina', 'Caesar']

absentGuest = guests.pop(2)
guests.append('Jonathan')
guests.insert(0, 'Daniel')
guests.insert(2, 'Elizabeth')
guests.insert(5, 'Robert')

for guest in guests:
	print(inviteMsg(guest))
	print("O convidado ", absentGuest, "não poderá comparecer.")
	print("Foi encontrada uma mesa maior, teremos mais convidados.")

print("Tem ", len(guests), " convidados no total.")