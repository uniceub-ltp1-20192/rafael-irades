def inviteMsg(name):
	msg = "Olá, ", name, "você foi convidado para um jantar."

	return msg

def uninviteMsg(name):
	msg = "Olá, ", name, ", infelizmente teremos que cancelar o seu convite para o jantar por falta de espaço."

	return msg

guests = ['George', 'Erina', 'Caesar']

guests.append('Jonathan')
guests.insert(0, 'Daniel')
guests.insert(2, 'Elizabeth')
guests.insert(5, 'Robert')

for i in range(0, len(guests) - 2):
	uninvitedGuest = guests.pop()
	print(uninviteMsg(uninvitedGuest))

for i in range(0, len(guests)):
	print(inviteMsg(guests[i]))
	print("A mesa encomendada para o jantar não chegará a tempo, poderemos ter apenas 2 convidados.")

del guests[1]
del guests[0]

print(guests)