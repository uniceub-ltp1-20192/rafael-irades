places = ['Warsaw', 'Rome', 'Venezia', 'Tokyo', 'Moscow']

print("Lista original:\n", places)
print("Lista organizada em ordem alfabética:\n", sorted(places))
print("Lista original:\n", places)

places.reverse()

print("Lista ao contrário:\n", places)

places.reverse()

print("Lista original:\n", places)

places.sort()

print("Lista em ordem alfabética:\n", places)

places.sort(reverse=True)

print("Lista em ordem alfabética reversa:\n", places)