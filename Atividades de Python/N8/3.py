countries = ['Poland', 'Japan', 'United States', 'Italy', 'Russia']

print("Lista original:\n", countries)
print("Lista organizada em ordem alfabética:\n", sorted(countries))

countries.reverse()

print("Lista original ao contrário:\n", countries)

countries.sort(reverse=True)

print("Lista em ordem alfabética ao contrário:\n", countries)

countries.sort()

print("Lista em ordem alfabética:\n", countries)