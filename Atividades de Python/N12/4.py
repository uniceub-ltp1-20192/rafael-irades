def printList(x):
	print("\nCardápio:")
	
	for i in x:
		print(i.title())

comidas = ('strogonoff', 'feijao', 'spaghetti', 'lasanha', 'arroz')

printList(comidas)

comidas = list(comidas)
comidas = comidas[:3]

comidas.append('risotto')
comidas.append('frango')

comidas = tuple(comidas)

printList(comidas)

print("\nErro:")
comidas[4] = 'arroz'