import sys

def printResult(msg, res):

	msg = msg + str(res)

	return msg

num_one = input("Digite o primeiro numero para a calculadora: ")
num_two = input("Digite o segundo numero para a calculadora: ")

print(printResult("O resultado da adicao e ", int(num_one) + int(num_two)))
print(printResult("O resultado da substracao e ", int(num_one) - int(num_two)))
print(printResult("O resultado da multiplicacao e ", int(num_one) * int(num_two)))

if num_two == 0:
	sys.exit("Nao pode ter divisao por 0")

print(printResult("O resultado da divisao e ", int(num_one) / int(num_two)))