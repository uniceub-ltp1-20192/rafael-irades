cebolas = 300 #Inicializa a variavel cebolas com valor 300
cebolas_na_caixa = 120 #Inicializa cebolas_na_caixa com valor 120
espaco_caixa = 5 #Inicializa espaco_caixa com valor 5
caixas = 60 #Inicializa caixas com valor 60
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa #Inicializa cebolas_na_caixa que tem valor igual a quantidade de cebolas menos a quantidade de cebolas em caixas
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa) #Inicializa caixas_vazias que tem como valor a quantidade de caixas menos a divisão entre a quantidade de cebolas em caixas pelo espaço das caixas
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa #Inicializa caixas_necessarios que tem como valor a quantidade de cebolas fora da caixa dividido pelo espaço das caixas

print ("Existem ", cebolas_na_caixa, " cebolas encaixotadas") #Mostra na tema quantas cebolas estão em caixas
print ("Existem ", cebolas_fora_da_caixa, " cebolas sem caixa") #Mostra na tela quantas cebolas não estão em caixas
print ("Em cada caixa cabem", espaco_caixa, " cebolas") #Mostra na tela quantas cebolas cabem em cada caixa
print ("Ainda temos ", caixas_vazias, " caixas vazias") #Mostra na tela quantas caixas estão vazias
print ("Então precisamos de ", caixas_necessarias, " caixas para empacotar todas as cebolas") #Mostra na tela quantas caixas ainda são necessárias para empacotas todas as cebolas