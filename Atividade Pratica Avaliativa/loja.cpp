#include <iostream>
	
	using namespace std;
	
	int addProduct() // Pergunta se quer adicionar produtos
	{
		int yn = 0;
		int q = 0;
		
		do{
			cout << "Há " << q << " produtos na lista." << endl;
			cout << "Quer adicionar mais um produto? (Digite 1 para sim e 0 para não)" << endl;
			cin >> yn;
			
			if (yn != 1 || 0)
			{
				cout << "Usuário não digitou 1 nem 0." << endl;
				yn = 0;
			}
			
			if (yn == 1)
			{
				q += 1;
			}
			
		} while (yn == 1);
		return q;
	}
	
	float askFloat (string s) // Retorna um float que o usuario escolher
	{
		float x = 0;
		
		cout << s << endl;
		cin >> x;

		return x;
	}
	
	int main() 
	{
		int productQuantity = 0;
		float totalValue = 0; 
		float totalDiscounted = 0;
	
		productQuantity = addProduct();
	
		for (int i = 0; i < productQuantity; i++)
		{
		    float productValue, productDiscount;
		    
			productValue = askFloat("Qual o valor do produto?");
			productDiscount = askFloat("Qual a porcentagem do desconto do produto Ex:(0.1 = 10%)");
			
			totalValue = totalValue + productValue;
			totalDiscounted = totalDiscounted + (productValue * (1 - productDiscount));
		}
		
		cout << "O valor total é " << totalValue << endl << "O valor com desconto é " << totalDiscounted;
	
		return 0;
	}
