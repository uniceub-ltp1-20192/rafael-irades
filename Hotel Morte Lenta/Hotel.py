'''
#No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
#A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
#Descubra quem é o assassino sabendo que:
#        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
#        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
#        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
# Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.
'''


import sys
import datetime


def askName():
    return input("Nome: ")

def validateString(s):
    if s.replace(' ', '').isalpha():
        return True
    
    print("O nome não pode ter numeros ou caracteres especiais.")
    
    return False

def validateVowels(s):
    numVowels = 0
    ou = False

    for char in s:
        if char in "aeiouAEIOU":
           numVowels = numVowels + 1

    if 'o' in s.lower() or 'u' in s.lower():
        ou = True

    if numVowels >= 3 and not ou:
        return True

    print("O nome tem menos de 3 vogais ou contem O ou U, o suspeito não é o assassino.")
    
    return False

def getBioGender(s):
    gender = 'M'

    if s.lower() == "maria calice" or "roberta corda":
        gender = 'F'
    
    return gender

def getWeapon(s):
    weapon = False
    if " arma" in s.lower():
        weapon = True

    return weapon

def validateAssassinWoman(g, w):
    if g == 'F' and w == False:
        return True

    return False

def validateAssassinMan(g, t):
    if g == 'M' and (t >= datetime.time(0, 30, 0) and t < datetime.time(0, 31, 0)):
        return True
    
    return False
    
# solcitar nome ao usuário
name = askName()

# verifica se o nome é uma string
vn = validateString(name)

if not vn:
	sys.exit("Este não é um nome válido.")

# verifica se nome respeita a regra das vogais
vv = validateVowels(name)

if not vv:
	sys.exit("Não é o assassino.")

# retorna M se o genero do suspeito for homem e F se for mulher
biogender = getBioGender(name)
# retorna False se for uma arma branca
weapon = getWeapon(name) 

# usa a hora do sistema
time = datetime.datetime.now().time()

vw = validateAssassinWoman(biogender, weapon)
vm = validateAssassinMan(biogender, time)

if not(vw or vm):
	sys.exit("Não é o assassino.")

print("O suspeito(a) é o assassino!")
