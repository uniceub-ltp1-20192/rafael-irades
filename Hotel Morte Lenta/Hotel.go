package main

import (
	"fmt"
	"time"
	"unicode"
	"strings"
	"bufio"
	"os"
)

func main() {
	name := askName() // Solicita o nome ao usuário
	// Verifica se o nome é uma string
	vn := validateString(name)

	if (!vn) {
		fmt.Println("Este não é um nome válido.")

		return
	}
	// Verifica a regra das vogais
	vv := validateVowels(name)

	if (!vv) {
		fmt.Println("O suspeito não é o assassino.")

		return
	}

	biogender := getBioGender(name) // Retorna M ou F dependendo do genero do suspeito
	weapon := getWeapon(name) // Retorna false se for uma arma branca

	vw := validadeAssassinWoman(biogender, weapon)
	vm := validadeAssassinMan(biogender)

	if !(vw || vm) {
		fmt.Println("O suspeito não é o assassino.")

		return
	}

	fmt.Println("O suspeito é o assassino!")
}

func askName() (n string){
	fmt.Print("Nome:")
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')

	n = text

	return
}

func validateString(n string) (vn bool) {
    for _, r := range n {
        if !unicode.IsLetter(r) && !unicode.IsSpace(r) {
            return false 
        }
    }

    return true
}

func validateVowels(n string) (vv bool) {
	vowelCounter := 0
	ou := false

	for _, value := range n {
    	switch value {
    	case 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U':
        	vowelCounter++
    	}
	}

	if (strings.ContainsAny(strings.ToLower(n), "uo")) {
		ou = true

		fmt.Println("O nome não pode ter O ou U")
	}

	if (vowelCounter < 3 || ou == true) {
		return false
	}

	return true
}

func getBioGender(n string) (gender string) {
	gender = "M"

	if (strings.Contains(strings.ToLower(n), "maria")) {
		gender = "F"
	}

	return
}

func getWeapon(n string) (weapon bool) {
	weapon = false

	if (strings.Contains(strings.ToLower(n), " arma")) {
		return true
	}

	return
}

func validadeAssassinWoman(g string, w bool) (vw bool){
	if (g == "F" && w == false) {
		return true
	}

	return false
}

func validadeAssassinMan(g string) (vm bool) {
	start, _ := time.Parse(time.Kitchen, "0:30AM")
  	end, _ := time.Parse(time.Kitchen, "0:31AM")
	time := time.Now() // Usa a hora do sistema
  	tf := false
	
	if inTimeSpan(start, end, time) {
        	tf = true
    	}

    	if (g == "M" && tf) {
    		return true
    	}

    	return false
}

func inTimeSpan(start, end, check time.Time) bool {
    return check.After(start) && check.Before(end)
}
